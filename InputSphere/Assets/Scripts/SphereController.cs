﻿using System;
using UnityEngine;

//[RequireComponent(typeof(InputHandler))]
public class SphereController : MonoBehaviour
{
    [SerializeField]private float _moveSpeed;
    [SerializeField]private InputHandler _inputHandler;
    [SerializeField] private GameObject _cubePrefab;
    private void OnEnable()
    {
        _inputHandler.InputAction += InputAction;
    }
    private void OnDisable()
    {
        _inputHandler.InputAction -= InputAction;
    }
    private void InputAction(KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.W: Move(Vector3.up); break;
            case KeyCode.S: Move(Vector3.down); break;
            case KeyCode.A: Move(Vector3.left); break;
            case KeyCode.D: Move(Vector3.right); break;
            case KeyCode.Space: Paint(); break;
        }
    }
    private void Paint()
    {
        Vector3 vec = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1f);
        Instantiate(_cubePrefab, vec, Quaternion.identity);
    }

    private void Move(Vector3 direction)
    {
        transform.Translate(_moveSpeed * Time.deltaTime * direction);
    }
}
