﻿using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public System.Action<KeyCode> InputAction = null;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InputAction?.Invoke(KeyCode.Space);
        }
        if (Input.GetKey(KeyCode.W))
        {
            InputAction?.Invoke(KeyCode.W);
        }
        if (Input.GetKey(KeyCode.S))
        {
            InputAction?.Invoke(KeyCode.S);
        }
        if (Input.GetKey(KeyCode.A))
        {
            InputAction?.Invoke(KeyCode.A);
        }
        if (Input.GetKey(KeyCode.D))
        {
            InputAction?.Invoke(KeyCode.D);
        }
    }
}
